package com.revature.dao_test;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.revature.dao.AccountDAO;
import com.revature.exceptions.NoSQLResultsException;
import com.revature.models.Account;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.class)

public class AccountDAO_Test {

	AccountDAO sut;

	@Mock
	Connection mockConn;

	@Mock
	PreparedStatement mockStatement;

	@Mock
	ResultSet mockRs;

	@Before
	public void setUp() {
		sut = new AccountDAO(mockConn);
	}

	@After
	public void tearDown() {
		sut = null;
	}

	@Test
	public void test_getAccounts() throws SQLException {
		ArrayList<Account> accounts = new ArrayList<Account>();

		int id = 1;
		double amount = 1;
		String type = "checking";
		Account account = new Account();
		String sql = "Select * From accounts WHERE accountHolderID = (?)";
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setInt(1, id);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);
		Mockito.when(mockRs.next()).thenReturn(true, false);
		Mockito.when(mockRs.getInt("AccountHolderID")).thenReturn(id);
		Mockito.when(mockRs.getDouble("Balance")).thenReturn(amount);
		Mockito.when(mockRs.getString("AccountType")).thenReturn(type);
		accounts.add(account);

		accounts = sut.getAccounts(1);

		Assert.assertEquals(id, accounts.get(0).getAccountNumber());
		Assert.assertEquals(amount, accounts.get(0).getBalance());
		Assert.assertEquals(type, accounts.get(0).getTypeOfAccount());

		verify(mockStatement, times(1)).executeQuery();
		verify(mockRs, times(1)).next();
		verify(mockRs, times(1)).getInt("AccountHolderID");
		verify(mockRs, times(1)).getDouble("Balance");
		verify(mockRs, times(1)).getString("AccountType");

	}

	@Test
	public void test_getAccount() throws SQLException {
		int id = 1;
		int acc = 1;
		double amount = 1;
		String type = "checking";
		String sql = "select * from accounts where accountholderid = (?) and accountid = (?)";
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setInt(1, id);
		Mockito.doNothing().when(mockStatement).setInt(2, acc);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);
		Mockito.when(mockRs.first()).thenReturn(true);
		Mockito.when(mockRs.getString("AccountType")).thenReturn(type);
		Mockito.when(mockRs.getDouble("Balance")).thenReturn(amount);
		Mockito.when(mockRs.getInt("AccountID")).thenReturn(id);

		Account account = sut.getAcc(1, 1);

		Assert.assertEquals(id, account.getAccountNumber());
		Assert.assertEquals(amount, account.getBalance());
		Assert.assertEquals(type, account.getTypeOfAccount());

		verify(mockStatement, times(1)).executeQuery();
		verify(mockRs, times(1)).first();
		verify(mockRs, times(1)).getInt("AccountID");
		verify(mockRs, times(1)).getDouble("Balance");
		verify(mockRs, times(1)).getString("AccountType");
	}

	@Test
	public void testPass_addAccount() throws SQLException, NoSQLResultsException {

		Account account = new Account();
		int id = 1;
		double balance = 1;
		String type = "checking";
		String sql = "Select * From accountholders WHERE accountHolderID = (?)";
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setInt(1, id);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);
		Mockito.when(mockRs.next()).thenReturn(true);
		sql = "insert into accounts (accountHolderID, Balance, AccountType) values ((?), (?), (?))";
		Mockito.doNothing().when(mockStatement).setInt(1, id);
		Mockito.doNothing().when(mockStatement).setDouble(2, balance);
		Mockito.doNothing().when(mockStatement).setString(3, type);
		Mockito.when(mockStatement.executeUpdate()).thenReturn(1);
		Mockito.when(mockStatement.getResultSet()).thenReturn(mockRs);

		verify(mockStatement, times(1)).executeUpdate();
		verify(mockStatement, times(1)).executeQuery();
		verify(mockRs, times(1)).next();
		verify(mockStatement, times(1)).setInt(1, id);
		verify(mockStatement, times(1)).setDouble(2, balance);
		verify(mockStatement, times(1)).setString(3, type);
		verify(mockStatement, times(1)).getResultSet();

	}


	@Test
	public void test_getAccountRange() throws SQLException {
		int id = 1;
		int top = 2000;
		int bot = 400;
		double balance = 1;
		String type = "checking";
		ArrayList<Account> accounts = new ArrayList<Account>();
		String sql = "select * from accounts where accountholderid = (?) and balance < (?) and balance > (?)";
		;
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setInt(1, id);
		Mockito.doNothing().when(mockStatement).setInt(2, top);
		Mockito.doNothing().when(mockStatement).setInt(3, bot);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);
		Mockito.when(mockRs.next()).thenReturn(true, false);
		Account account = new Account();
		Mockito.when(mockRs.getString("AccountType")).thenReturn(type);
		Mockito.when(mockRs.getDouble("Balance")).thenReturn(balance);
		Mockito.when(mockRs.getInt("AccountId")).thenReturn(id);
		accounts.add(account);

		accounts = sut.getAccountRange(id, top, bot);

		Assert.assertEquals(id, accounts.get(0).getAccountNumber());
		Assert.assertEquals(balance, accounts.get(0).getBalance());
		Assert.assertEquals(type, accounts.get(0).getTypeOfAccount());

		verify(mockStatement, times(1)).executeQuery();
		verify(mockRs, times(2)).next();
		verify(mockRs, times(1)).getInt("AccountHolderID");
		verify(mockRs, times(1)).getDouble("Balance");
		verify(mockRs, times(1)).getString("AccountType");
		verify(mockStatement, times(1)).setInt(1, id);
		verify(mockStatement, times(1)).setInt(2, top);
		verify(mockStatement, times(1)).setInt(3, bot);
	}

	@Test
	public void updateType_test() throws SQLException {
		String sql = "Update accounts set AccountType = (?) Where accountholderid = (?) and accountID = (?)";
		int id = 1;
		Account account = new Account();
		int accId = 1;
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setString(1, account.getTypeOfAccount());
		Mockito.doNothing().when(mockStatement).setInt(2, id);
		Mockito.doNothing().when(mockStatement).setInt(3, accId);
		Mockito.when(mockStatement.executeUpdate()).thenReturn(1);
		Mockito.when(mockStatement.getResultSet()).thenReturn(mockRs);

		verify(mockStatement, times(1)).executeQuery();
		verify(mockStatement, times(1)).setString(1, account.getTypeOfAccount());
		verify(mockStatement, times(1)).setInt(2, id);
		verify(mockStatement, times(1)).setInt(3, accId);
		verify(mockStatement, times(1)).executeUpdate();
		verify(mockStatement, times(1)).getResultSet();
	}

	@Test
	public void deleteAcc_test() throws SQLException {
		String sql = "Select name from accountholders where accountholderId = (?)";
		int id = 1;
		int accId = 1;
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setInt(1, id);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);

		sql = "Select * from accounts where accountId = (?)";
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setInt(1, accId);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);

		sql = "delete from accounts where AccountHolderID = (?) and AccountID = (?)";
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setInt(1, id);
		Mockito.doNothing().when(mockStatement).setInt(2, accId);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);

		// verify(mockStatement, times(3)).executeQuery();
		verify(mockStatement, times(2)).setInt(1, id);
		verify(mockStatement, times(1)).setInt(1, accId);
		verify(mockStatement, times(1)).setInt(2, accId);
		verify(mockStatement, times(3)).executeQuery();

	}

	@Test
	public void deposit_test() throws SQLException {
		Account account = new Account();
		int id = 1;
		int accId = 1;
		double amount = 12;
		double balance = 12;
		String sql = "Select * from accounts where accountholderId = (?) and accountid = (?)";
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setInt(1, id);
		Mockito.doNothing().when(mockStatement).setInt(2, accId);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);
		Mockito.when(mockRs.first()).thenReturn(true);
		Mockito.when(mockRs.getDouble("Balance")).thenReturn(balance);
		account.setBalance(balance);
		account.deposit(amount);
		sql = "Update accounts set Balance = (?) Where accountholderid = (?) and accountID = (?)";
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setDouble(1, balance);
		Mockito.doNothing().when(mockStatement).setInt(2, id);
		Mockito.doNothing().when(mockStatement).setInt(3, accId);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);

		Assert.assertEquals(24, account.getBalance());

	}

	public void withdraw_test() throws SQLException {
		Account account = new Account();
		int id = 1;
		int accId = 1;
		double amount = 12;
		double balance = 13;
		String sql = "Select * from accounts where accountholderId = (?) and accountid = (?)";
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setInt(1, id);
		Mockito.doNothing().when(mockStatement).setInt(2, accId);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);
		Mockito.when(mockRs.first()).thenReturn(true);
		Mockito.when(mockRs.getDouble("Balance")).thenReturn(balance);
		account.setBalance(balance);
		if (account.getBalance() - amount > 0)
			;
		account.withdraw(amount);
		sql = "Update accounts set Balance = (?) Where accountholderid = (?) and accountID = (?)";
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setDouble(1, balance);
		Mockito.doNothing().when(mockStatement).setInt(2, id);
		Mockito.doNothing().when(mockStatement).setInt(3, accId);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);

		Assert.assertEquals(1, account.getBalance());

		verify(mockRs, times(1)).first();
		verify(mockStatement, times(1)).setInt(1, id);
		verify(mockStatement, times(1)).setInt(2, accId);
		verify(mockStatement, times(1)).setDouble(1, balance);
		verify(mockStatement, times(1)).setInt(2, id);
		verify(mockStatement, times(1)).setInt(3, accId);

	}
	
	@Test
	public void transfer_test() throws SQLException {
		Account account1 = new Account();
		Account account2 = new Account();
		int id = 1;
		int accId = 1;
		double amount = 6;
		double balance = 12;
		String sql = "Select * from accounts where accountholderId = (?) and accountid = (?)";
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setInt(1, id);
		Mockito.doNothing().when(mockStatement).setInt(2, accId);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);
		String sqlSecond = "Select * from accounts where accountholderId = (?) and accountid = (?)";
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setInt(1, id);
		Mockito.doNothing().when(mockStatement).setInt(2, accId);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);
		Mockito.when(mockRs.first()).thenReturn(true);
		account1.setBalance(balance);
		account2.setBalance(balance);
		if(account1.getBalance() - amount > 0) {
			account1.withdraw(amount);
			sql = "Update accounts set Balance = (?) Where accountholderid = (?) and accountID = (?)";
			Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
			Mockito.doNothing().when(mockStatement).setDouble(1, balance);
			Mockito.doNothing().when(mockStatement).setInt(2, id);
			Mockito.doNothing().when(mockStatement).setInt(3, accId);
			Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);
			account2.deposit(amount);
			sql = "Update accounts set Balance = (?) Where accountholderid = (?) and accountID = (?)";
			Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
			Mockito.doNothing().when(mockStatement).setDouble(1, balance);
			Mockito.doNothing().when(mockStatement).setInt(2, id);
			Mockito.doNothing().when(mockStatement).setInt(3, accId);
			Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);
		}
		
		Assert.assertEquals(6.0, account1.getBalance());
		Assert.assertEquals(18.0, account2.getBalance());
		
		verify(mockRs, times(1)).first();
		verify(mockStatement, times(2)).setInt(1, id);
		verify(mockStatement, times(2)).setInt(2, accId);
		verify(mockStatement, times(2)).setDouble(1, balance);
		verify(mockStatement, times(2)).setInt(2, id);
		verify(mockStatement, times(2)).setInt(3, accId);

		
	}

}
