package com.revature.dao_test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.revature.dao.AccountDAO;
import com.revature.dao.AccountHolderDAO;
import com.revature.exceptions.NoSQLResultsException;
import com.revature.models.Account;
import com.revature.models.AccountHolder;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.class)

public class AccountHolderDAO_test {

	AccountHolderDAO sut;

	@Mock
	Connection mockConn;

	@Mock
	PreparedStatement mockStatement;

	@Mock
	ResultSet mockRs;

	@Before
	public void setUp() {
		sut = new AccountHolderDAO(mockConn);
	}

	@After
	public void tearDown() {
		sut = null;
	}

	@Test
	public void get_test() throws SQLException {
		int id = 1;
		String name = "n";
		AccountHolder ah = new AccountHolder();
		String sql = "Select * From accountholders Where accountholderid = ?";
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setInt(1, id);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);
		Mockito.when(mockRs.first()).thenReturn(true);
		Mockito.when(mockRs.getInt("accountholderId")).thenReturn(id);
		Mockito.when(mockRs.getString("name")).thenReturn(name);
		ah.setAccountNumber(id);
		ah.setName(name);
		ah = sut.get(1);

		Assert.assertEquals(id, ah.getAccountNumber());
		Assert.assertEquals(name, ah.getName());

		verify(mockStatement, times(1)).executeQuery();
		verify(mockRs, times(1)).first();
		verify(mockRs, times(1)).getInt("AccountNumber");
		verify(mockRs, times(1)).getString("name");

	}

	@Test
	public void getAll_test() throws SQLException {
		String sql = "Select * From accountholders";
		int id = 1;
		String name = "n";
		List<AccountHolder> accounts = new ArrayList<AccountHolder>();

		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.when(mockStatement.executeQuery()).thenReturn(mockRs);
		Mockito.when(mockRs.next()).thenReturn(true, false);
		AccountHolder ah = new AccountHolder();
		Mockito.when(mockRs.getInt("AccountNumber")).thenReturn(id);
		Mockito.when(mockRs.getString("Name")).thenReturn(name);
		accounts.add(ah);

		accounts = sut.getAll();

		Assert.assertEquals(id, accounts.get(0).getAccountNumber());
		Assert.assertEquals(name, accounts.get(0).getName());

		verify(mockConn, times(1)).prepareStatement(sql);
		verify(mockStatement, times(1)).executeQuery();
		verify(mockRs, times(1)).next();
		verify(mockRs, times(1)).getInt("AccountNumber");
		verify(mockRs, times(1)).getString("Name");

	}

	@Test
	public void add_test() throws SQLException {
		String sql = "Insert into accountholders (name) values (?)";
		String name = "n";
		AccountHolder ah = new AccountHolder(name, 1);
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setString(1, name);
		Mockito.when(mockStatement.executeUpdate()).thenReturn(1);
		Mockito.when(mockStatement.getResultSet()).thenReturn(mockRs);
		

		Assert.assertEquals(name, ah.getName());

		verify(mockConn, times(1)).prepareStatement(sql);
		verify(mockStatement, times(1)).setString(1, name);
		verify(mockStatement, times(1)).executeUpdate();
		verify(mockStatement, times(1)).getResultSet();

	}

	@Test
	public void update_test() throws SQLException {
		String sql = "Update accountholders set name = (?) Where accountholderid = (?)";
		int id = 1;
		String name = "N";
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setString(1, name);
		Mockito.doNothing().when(mockStatement).setInt(2, id);
		Mockito.when(mockStatement.executeUpdate()).thenReturn(1);
		Mockito.when(mockStatement.getResultSet()).thenReturn(mockRs);
		
		verify(mockConn, times(1)).prepareStatement(sql);
		verify(mockStatement, times(1)).setString(1, name);
		verify(mockStatement, times(1)).setInt(2, id);
		verify(mockStatement, times(1)).executeUpdate();
		verify(mockStatement, times(1)).getResultSet();
	}

	@Test
	public void delete_test() throws SQLException {
		String sql = "delete from accountholders where AccountHolderID = (?)";
		int id = 1;
		Mockito.when(mockConn.prepareStatement(sql)).thenReturn(mockStatement);
		Mockito.doNothing().when(mockStatement).setInt(1, id);
		Mockito.when(mockStatement.executeUpdate()).thenReturn(1);
		Mockito.when(mockStatement.getResultSet()).thenReturn(mockRs);
		
		verify(mockConn, times(1)).prepareStatement(sql);
		verify(mockStatement, times(1)).setInt(1, id);
		verify(mockStatement, times(1)).executeUpdate();
		verify(mockStatement, times(1)).getResultSet();
	}
}