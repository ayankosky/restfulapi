package com.revature.endpoints;
import io.javalin.http.Context;
import io.javalin.Javalin;
import com.revature.controllers.AccountHolderController;
import com.revature.controllers.*;
public class EndPointInit {
	
	private static Javalin javalin;
	
	public static void init(Javalin app) {
        javalin = app;
        app.get("/accountholder/accounts/:id", AccountController::getAccountRan);
        app.get("/accountholder/:id/accounts", AccountController::getAll);
        app.get("/accountholder/:id", AccountHolderController::getById);
        app.get("/accountholder", AccountHolderController::getAll);
        app.post("/accountholder", AccountHolderController::add);
        app.put("/accountholder/:id", AccountHolderController::update);
        app.delete("/accountholder/:id", AccountHolderController::delete);
        app.post("/accountholder/accounts/:id", AccountController::addAccount);
        app.get("/accountholder/:id/accounts/:an", AccountController::getAccount);
        app.put("accountholder/:id/accounts/:acc", AccountController::updateAccount);
        app.delete("/accountholder/:id/accounts/:acc", AccountController::deleteAccount);
        app.patch("/accountholder/:id/accounts/:acc", AccountController::transaction);
        app.patch("/accountholder/:id/accounts/:acc/transfer/:tran", AccountController::transferFunds);
        

    }
	
	

}
