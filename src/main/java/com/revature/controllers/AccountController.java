package com.revature.controllers;

import java.sql.SQLException;

import com.revature.dao.AccountDAO;
import com.revature.dao.AccountHolderDAO;
import com.revature.exceptions.InsufficentFundsException;
import com.revature.exceptions.NoSQLResultsException;
import com.revature.models.Account;
import com.revature.models.AccountHolder;
import com.revature.utils.ConnectionFactory;

import io.javalin.http.Context;

public class AccountController {

	public static void getAll(Context ctx) {
		AccountDAO dao = new AccountDAO(ConnectionFactory.getConnection());
		int id = Integer.parseInt(ctx.pathParam("id"));

		try {
			ctx.json(dao.getAccounts(id));

		} catch (NoSQLResultsException e) {
			ctx.status(404);
			e.printStackTrace();
		} catch (SQLException e) {
			ctx.status(404);
			e.printStackTrace();
		}
	}

	public static void addAccount(Context ctx) {
		AccountDAO dao = new AccountDAO(ConnectionFactory.getConnection());
		int id = Integer.parseInt(ctx.pathParam("id"));
		Account account = ctx.bodyAsClass(Account.class);

		try {
			dao.addAccount(id, account);
			ctx.status(201);
		} catch (NoSQLResultsException e) {
			ctx.status(404);
			e.printStackTrace();
		} catch (SQLException e) {
			ctx.status(404);
			e.printStackTrace();
		}
	}

	public static void getAccount(Context ctx) {
		AccountDAO dao = new AccountDAO(ConnectionFactory.getConnection());
		int id = Integer.parseInt(ctx.pathParam("id"));
		int accountNum = Integer.parseInt(ctx.pathParam("an"));
		System.out.println(accountNum);

		try {
			ctx.json(dao.getAcc(id, accountNum));
		}
		catch (NoSQLResultsException e) {
			ctx.status(404);
			e.printStackTrace();
		} 
		catch (SQLException e) {
			ctx.status(404);
		}
	}

	public static void getAccountRan(Context ctx) {
		AccountDAO dao = new AccountDAO(ConnectionFactory.getConnection());
		int id = Integer.parseInt(ctx.pathParam("id"));
		int top = Integer.parseInt(ctx.queryParam("amountLessThan"));
		int bot = Integer.parseInt(ctx.queryParam("amountGreaterThan"));

		try {
			ctx.json(dao.getAccountRange(id, top, bot));

		} catch (NoSQLResultsException e) {
			ctx.status(404);
			e.printStackTrace();
		} catch (SQLException e) {
			ctx.status(404);
			e.printStackTrace();
		} 
	}

	public static void updateAccount(Context ctx) {
		AccountDAO dao = new AccountDAO(ConnectionFactory.getConnection());
		Account account = ctx.bodyAsClass(Account.class);
		int id = Integer.parseInt(ctx.pathParam("id"));
		int accId = Integer.parseInt(ctx.pathParam("acc"));
		try {
			dao.updateType(account, id, accId);
		} catch (NoSQLResultsException e) {
			ctx.status(404);
			e.printStackTrace();
		} catch (SQLException e) {
			ctx.status(404);
			e.printStackTrace();
		} catch (NullPointerException e) {
			ctx.status(404);
		}
	}

	public static void deleteAccount(Context ctx) throws SQLException, NoSQLResultsException {
		AccountDAO dao = new AccountDAO(ConnectionFactory.getConnection());
		int id = Integer.parseInt(ctx.pathParam("id"));
		int accId = Integer.parseInt(ctx.pathParam("acc"));
		try {
			dao.deleteAcc(id, accId);
		} catch (SQLException e) {
			ctx.status(404);
			e.printStackTrace();
		} catch (NoSQLResultsException e) {
			ctx.status(404);
			e.printStackTrace();
		}
	}

	public static void transaction(Context ctx) throws SQLException, InsufficentFundsException, NoSQLResultsException {
		AccountDAO dao = new AccountDAO(ConnectionFactory.getConnection());
		int id = Integer.parseInt(ctx.pathParam("id"));
		int accId = Integer.parseInt(ctx.pathParam("acc"));
		String str = ctx.body();

		String[] transType = str.split("\\s+");

		if (transType[1].equals("\"deposit\"")) {
			Double amount = Double.parseDouble(transType[3]);
			try {
				dao.deposit(id, accId, amount);
			} catch (NoSQLResultsException e) {
				ctx.status(404);
				e.printStackTrace();
			} catch (SQLException e) {
				ctx.status(404);
				e.printStackTrace();
			}
		} else {

			Double amount = Double.parseDouble(transType[3]);
			try {
				dao.withdraw(id, accId, amount);
			} catch (NoSQLResultsException e) {
				ctx.status(404);
				e.printStackTrace();
			} catch (InsufficentFundsException e) {
				ctx.status(422);
				e.printStackTrace();
			}
		}
	}

	public static void transferFunds(Context ctx)
			throws SQLException, InsufficentFundsException, NoSQLResultsException {
		AccountDAO dao = new AccountDAO(ConnectionFactory.getConnection());
		int id = Integer.parseInt(ctx.pathParam("id"));
		int accId = Integer.parseInt(ctx.pathParam("acc"));
		int transferId = Integer.parseInt(ctx.pathParam("tran"));
		String str = ctx.body();

		String[] transType = str.split("\\s+");

		Double amount = Double.parseDouble(transType[3]);
		try {
			dao.transfer(id, accId, transferId, amount);
		} catch (InsufficentFundsException e) {
			ctx.status(422);
			e.printStackTrace();
		} catch (NoSQLResultsException e) {
			ctx.status(404);
			e.printStackTrace();
		} catch (SQLException e) {
			ctx.status(404);
			e.printStackTrace();
		}

	}

//	public static void addChecing(Context ctx) throws SQLException {
//		AccountDAO dao = new AccountDAO(ConnectionFactory.getConnection());
//		int id = Integer.parseInt(ctx.pathParam("id"));
//		String str = "checking";
//		dao.addAccount(id, str);
//	}

}
