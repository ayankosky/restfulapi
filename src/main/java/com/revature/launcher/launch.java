package com.revature.launcher;

import java.sql.Connection;
import com.revature.controllers.AccountHolderController;
import com.revature.endpoints.EndPointInit;
import com.revature.utils.ConnectionFactory;

import io.javalin.Javalin;

public class launch {

	public static void main(String[] args) {
		Javalin app = Javalin.create().start(3000);
		Connection conn = ConnectionFactory.getConnection();
		
		EndPointInit.init(app);
		
		
		
		
	}

}
