package com.revature.dao;

import java.sql.SQLException;
import java.util.List;

import com.revature.exceptions.NoSQLResultsException;
import com.revature.models.AccountHolder;

public interface DAO<T> {

	T get(int id) throws SQLException, NoSQLResultsException;
	
	List<T> getAll() throws SQLException;
	
	void add(T t) throws SQLException;
	
	void update(int id) throws SQLException;
	
	void delete(int id) throws SQLException;

	
}
